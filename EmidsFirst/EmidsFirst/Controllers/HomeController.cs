﻿using EmidsFirst.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EmidsFirst.Controllers
{
    public class HomeController : Controller
    {
        private BusinessService businessService;

        public HomeController()
        {
            businessService = new BusinessService();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> WebSearch(string searchText)
        {
            var model = await businessService.GetSearchResults(searchText);
            return View("ResultPage", model);

        }

        [HttpGet]
        public async Task<JsonResult> AutoComplete(string searchText)
        {
            var result = await businessService.AutoComplete(searchText);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}