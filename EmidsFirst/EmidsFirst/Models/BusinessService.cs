﻿using DuckDuckGo.Net;
using Microsoft.SharePoint.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Security;
using System.Threading.Tasks;

namespace EmidsFirst.Models
{
    public class BusinessService
    {
        private readonly string SharepointUrl = "";
        private readonly string SharepointUsername = "";
        private readonly string SharepointPassword = "";
        private readonly string AutocompleteUrl = "";

        public BusinessService()
        {
            SharepointUrl = ConfigurationManager.AppSettings["SharepointUrl"].ToString();
            SharepointUsername = ConfigurationManager.AppSettings["SharepointUsername"].ToString();
            SharepointPassword = ConfigurationManager.AppSettings["SharepointPassword"].ToString();
            AutocompleteUrl = ConfigurationManager.AppSettings["AutoCompleteUrl"].ToString();
        }

        public async Task<List<SearchResult>> GetSearchResults(string serachText)
        {
            var searchResults = new List<SearchResult>();
            try
            {
                await AddSharepointResults(serachText, searchResults);
                AddBrowserResults(string.Format("{0}{1}{2}", "\'", serachText, "\'"), searchResults);
            }
            catch (Exception ex)
            {
                searchResults = new List<SearchResult>();
            }
            return searchResults;
        }

        private async Task AddSharepointResults(string serachText, List<SearchResult> searchResults)
        {
            var password = SharepointPassword;
            var securePassword = new SecureString();
            foreach (var c in password)
            {
                securePassword.AppendChar(c);
            }
            var credentials = new SharePointOnlineCredentials(SharepointUsername, securePassword);

            var request = WebRequest.Create(string.Format("{0}_api/search/query?querytext={1}{2}{3}&startrow=0&rowlimit=500", SharepointUrl, "\'", serachText, "\'")) as HttpWebRequest;
            request.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
            request.Credentials = credentials;
            request.ContentType = "application/json;odata=verbose";
            request.Accept = "application/json;odata=verbose";

            var response = (HttpWebResponse)(await request.GetResponseAsync());
            string json = string.Empty;
            using (var responseStream = response.GetResponseStream())
            {
                json = new StreamReader(responseStream).ReadToEnd();
            }
            var spResult = JsonConvert.DeserializeObject<SharepointResult>(json);
            if (spResult.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results.Count > 0)
            {
                foreach (var itemGroup in spResult.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results)
                {
                    var subItem = new SearchResult();
                    foreach (var item in itemGroup.Cells.results)
                    {
                        switch (item.Key)
                        {
                            case "Title":
                                subItem.Title = item.Value != null ? item.Value.ToString() : string.Empty;
                                break;
                            case "Path":
                                subItem.Path = item.Value != null ? item.Value.ToString() : string.Empty;
                                break;
                            case "LastModifiedTime":
                                subItem.LastModifiedTime = item.Value != null ? item.Value.ToString() : string.Empty;
                                break;
                            case "HitHighlightedSummary":
                                subItem.HitHighlightedSummary = item.Value != null ? item.Value.ToString() : string.Empty;
                                break;
                        }
                    }
                    searchResults.Add(subItem);
                }
            }
        }

        private void AddBrowserResults(string searchText, List<SearchResult> searchResults)
        {
            var webResults = new List<WebResult>();
            try
            {
                var search = new Search
                {
                    NoHtml = true,
                    NoRedirects = true,
                    IsSecure = true,
                    SkipDisambiguation = true,
                    ApiClient = new HttpWebApi()
                };
                var jsonString = search.TextQuery(searchText, "Google", ResponseFormat.Json);
                var result = JsonConvert.DeserializeObject<WebResults>(jsonString);
                if (result != null && result.RelatedTopics != null && result.RelatedTopics.Count > 0)
                {
                    foreach (var item in result.RelatedTopics)
                    {
                        if (string.IsNullOrEmpty(item.Text)) continue;
                        var subItem = new SearchResult
                        {
                            Title = item.Text,
                            Path = item.FirstURL,
                            HitHighlightedSummary = item.Result
                        };
                        searchResults.Add(subItem);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public async Task<List<AutocompleteModel>> AutoComplete(string searchText)
        {
            var result = new List<AutocompleteModel>();
            try
            {
                var request = WebRequest.Create(string.Format(AutocompleteUrl, searchText)) as HttpWebRequest;
                var response = (HttpWebResponse)(await request.GetResponseAsync());
                string json = string.Empty;
                using (var responseStream = response.GetResponseStream())
                {
                    json = new StreamReader(responseStream).ReadToEnd();
                }
                result = JsonConvert.DeserializeObject<List<AutocompleteModel>>(json);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class AutocompleteModel
    {
        public string phrase { get; set; }
    }

    public class SearchResult
    {
        public string Title { get; set; }
        public string Path { get; set; }
        public string LastModifiedTime { get; set; }
        public string HitHighlightedSummary { get; set; }
    }

    public class WebResult
    {
        public string Text { get; set; }
        public string FirstURL { get; set; }
        public string Result { get; set; }
    }

    public class WebResults
    {
        public List<WebResult> RelatedTopics { get; set; }
    }
}