﻿using System.Collections.Generic;

namespace EmidsFirst.Models
{
    public class Metadata
    {
        public string type { get; set; }
    }

    public class Metadata2
    {
        public string type { get; set; }
    }

    public class Metadata3
    {
        public string type { get; set; }
    }

    public class CustomResults
    {
        public Metadata3 __metadata { get; set; }
        public List<object> results { get; set; }
    }

    public class Metadata4
    {
        public string type { get; set; }
    }

    public class Metadata5
    {
        public string type { get; set; }
    }

    public class Result
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string ValueType { get; set; }
    }

    public class Properties
    {
        public Metadata5 __metadata { get; set; }
        public List<Result> results { get; set; }
    }

    public class Metadata6
    {
        public string type { get; set; }
    }

    public class Metadata7
    {
        public string type { get; set; }
    }

    public class Metadata8
    {
        public string type { get; set; }
    }

    public class Result3
    {
        public Metadata8 __metadata { get; set; }
        public string Key { get; set; }
        public object Value { get; set; }
        public string ValueType { get; set; }
    }

    public class Cells
    {
        public List<Result3> results { get; set; }
    }

    public class Result2
    {
        public Metadata7 __metadata { get; set; }
        public Cells Cells { get; set; }
    }

    public class Rows
    {
        public List<Result2> results { get; set; }
    }

    public class Table
    {
        public Metadata6 __metadata { get; set; }
        public Rows Rows { get; set; }
    }

    public class RelevantResults
    {
        public Metadata4 __metadata { get; set; }
        public object GroupTemplateId { get; set; }
        public object ItemTemplateId { get; set; }
        public Properties Properties { get; set; }
        public object ResultTitle { get; set; }
        public object ResultTitleUrl { get; set; }
        public int RowCount { get; set; }
        public Table Table { get; set; }
        public int TotalRows { get; set; }
        public int TotalRowsIncludingDuplicates { get; set; }
    }

    public class PrimaryQueryResult
    {
        public Metadata2 __metadata { get; set; }
        public CustomResults CustomResults { get; set; }
        public string QueryId { get; set; }
        public string QueryRuleId { get; set; }
        public object RefinementResults { get; set; }
        public RelevantResults RelevantResults { get; set; }
        public object SpecialTermResults { get; set; }
    }

    public class Metadata9
    {
        public string type { get; set; }
    }

    public class Result4
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string ValueType { get; set; }
    }

    public class Properties2
    {
        public Metadata9 __metadata { get; set; }
        public List<Result4> results { get; set; }
    }

    public class Metadata10
    {
        public string type { get; set; }
    }

    public class SecondaryQueryResults
    {
        public Metadata10 __metadata { get; set; }
        public List<object> results { get; set; }
    }

    public class Metadata11
    {
        public string type { get; set; }
    }

    public class TriggeredRules
    {
        public Metadata11 __metadata { get; set; }
        public List<object> results { get; set; }
    }

    public class Query
    {
        public Metadata __metadata { get; set; }
        public int ElapsedTime { get; set; }
        public PrimaryQueryResult PrimaryQueryResult { get; set; }
        public Properties2 Properties { get; set; }
        public SecondaryQueryResults SecondaryQueryResults { get; set; }
        public string SpellingSuggestion { get; set; }
        public TriggeredRules TriggeredRules { get; set; }
    }

    public class D
    {
        public Query query { get; set; }
    }

    public class SharepointResult
    {
        public D d { get; set; }
    }

}