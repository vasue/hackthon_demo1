﻿$(document).ready(function () {
    $("#searchTextField").autocomplete({
        source: []
    });
    $('#searchTextField').on('keyup', function () {
        $.ajax({
            url: $("#AutoCompleteUrl").val() +'?searchText='+ $(this).val(),
            async: true,
            success: function (result) {
                var items = [];
                $.each(result, function (key, val) {
                    items.push(val.phrase);
                });
                $("#searchTextField").autocomplete({
                    source: items
                });
            },
        });
    })

    $('#searchTextField').on('keypress', function (e) {
        if (e.which == 13) {
            $("#btn_Submit").click();
        }
    })

    $("#btn_Submit").click(function () {
        if ($("#searchTextField").val().length > 0)
            window.location.replace($("#SearchUrl").val() + '?searchText=' + $("#searchTextField").val());
        else
            $("#searchTextField").val('');
    });
});